---
hide:
  - toc
author: à compléter
title: Liste des challenges
---

# Programmation Python/Scratch



!!! abstract "Description générale de la catégorie"

    Dans cette catégorie, on propose divers challenges dont la résolution conduit à la lecture ou à l'écriture de programmes Python ou Scratch.

    
!!! warning "Autres challenges"

    D'autres challenges dans différentes catégories sont également disponibles sur le <a href='https://cybersecurite.forge.apps.education.fr/cyber/3.Challenges/presentation/' target='_blank'>site principal</a> dédié à la cybersécurité.

    Etes-vous prêt à relever les défis ?


<hr style="height:5px;color:red;background-color:red;">


!!! note "Age glaciaire"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 3 flags

        **Objectif :** comprendre et exécuter un programme écrit en Scratch

    ![](../ressources_challenges_prog/AgeGlace/age_glace.png){: .center }
    
    Sur la banquise, vivait un pingouin curieux nommé Pipou. Un jour, il découvrit un parchemin mystérieux indiquant l'emplacement d'un trésor. Aidez le à le trouver !

    [_Accéder au challenge_](../ressources_challenges_prog/AgeGlace/age_glace){ .md-button target="_blank" rel="noopener" }


!!! note "Art Expo (d'après <a href='https://www.101computing.net/'>101computing.net</a>)"

    !!! tip "Présentation"

        **Niveau :** NSI

        **Difficulté :** ⭐⭐ 12 flags

        **Objectif :** manipuler les listes Python

    ![](../ressources_challenges_prog/ArtExpo/art.png){: .center }
    
    La grande exposition Art Expo va bientôt être inaugurée ! Vous devez aidez le galeriste à accéder aux douze tableaux les plus célèbres du monde avant cette événement international !

    [_Accéder au challenge_](../ressources_challenges_prog/ArtExpo/art_expo){ .md-button target="_blank" rel="noopener" }

!!! note "Blackbeard’s Treasure Map (d'après <a href='https://www.101computing.net/'>101computing.net</a>)"

    !!! tip "Présentation"

        **Niveau :** collège, SNT, NSI

        **Difficulté :** ⭐ 1 flag

        **Objectif :** comprendre la technique de l'attaque par force brute

    ![](../ressources_challenges_prog/BlackbeardsTreasureMap/pirate-flag2.png){: .center }
    
    Différents fragments de parchemins viennent d'être découverts. Il semble qu'il indique l'emplacement du trésor du célèbre pirate Barbe-Noire. Saurez-vous faire preuve de sagacité et de perspicacité pour les déchiffer ?

    !!! warning "Challenge en anglais"
    ![](../ressources_challenges_prog/BlackbeardsTreasureMap/anglais1.png)

    [_Accéder au challenge_](../ressources_challenges_prog/BlackbeardsTreasureMap/BlackbeardsTreasureMap){ .md-button target="_blank" rel="noopener" }
