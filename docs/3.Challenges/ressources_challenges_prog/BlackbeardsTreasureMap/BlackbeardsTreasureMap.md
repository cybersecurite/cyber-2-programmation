---
hide:
  - toc
author: à compléter
title: Blackbeard’s Treasure Map
---

# Challenge : Blackbeard’s Treasure Map

![](../pirate-flag2.png){: .center }

We have all heard of the famous English pirate called Blackbeard who sailed the seven seas during the XVIII century. Through his numerous acts of piracy, Blackbeard accumulated a huge collection of riches including golden coins, jewels, golden plates and precious stones. Blackbeard was wise enough not to carry his treasure on his vessel. Instead he buried his treasure in a secret location, somewhere in the middle of the Pacific Ocean.

Recently, a team of SCUBA divers found a message in a bottle while SCUBA diving near Coral Bay, Western Australia. Inside this old bottle of rhum, they found two pieces of parchment believed to have belonged to Blackbeard.

<center><img src = '../message-in-a-bottle.png' style='width:10%;height:auto;'></center>


??? abstract "First parchment"

    The first piece of parchment is a map. Looking at the shapes of the three islands on the map, the SCUBA divers have been able to locate the location of these islands somewhere a few miles off the coast of Australia. They strongly believe that this map will help them find the exact location of Blackbeard’s treasure.

    <center><img src = '../Blackbeard-Treasure-Map.png' style='width:70%;height:auto;'></td></center>


??? abstract "Second parchment"

    The second parchment is believed to be the key to help locate the position of the treasure on the map.

    <center><img src = '../blackbeard-clue.png' style='width:65%;height:auto;'></td></center>


!!! note "Your task"

    Find the row number and the column number that indicate the location of the treasure.

    The flag is the concatenation of these two numbers.

    For example, if the row number is `1515` and the column number is `1492`, then the flag is `15151492`.


<hr style="height:5px;color:red;background-color:red;">
<center>It's your turn!</center>
<hr style="height:5px;color:red;background-color:red;">


<p>
    <form id="form_BlackbeardsTreasureMap">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
<p id='Texte1'></p>


<div id='Final' markdown="1" hidden>
<br>
<h2>Congratulations!</h2>
<p>Blackbeard's treasure is ours!</p>
<center><img src='../Blackbeard-Treasure.png' style='width:40%;height:auto;'></center>
<br>

!!! danger "A retenir"

    La méthode d'attaque par _force brute_ est une technique classique des pirates qui consiste à tester toutes les possibilités afin d'obtenir la bonne réponse. Elle est souvent utilisée pour casser des mots de passe.

</div>

<script src='../script_chall_BlackbeardsTreasureMap.js'></script>