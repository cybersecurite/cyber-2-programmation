const flags = {'art_expo1':['71624',1,1], // [flag, numéro de la partie dans le challenge, partie suivante ou pas]
				'art_expo2':['18325',2,1],
				'art_expo3':['102231',3,1],
                'art_expo4':['81624',4,1],
                'art_expo5':['17935',5,1],
                'art_expo6':['122312',6,1],
                'art_expo7':['122720',7,1],
                'art_expo8':['218',8,1],
                'art_expo9':['12249',9,1],
                'art_expo10':['741',10,1],
                'art_expo11':['753',11,1],
                'art_expo12':['24156',12,0]};

function check_and_update(event)
{
    event.preventDefault();

    const f = event.target;
    const f_name = f.id.substring(5); // Extraction de la clé du dictionnaire dans le nom du formulaire
    const texte = document.getElementById('Texte'+flags[f_name][1].toString());
    if (f.flag.value===flags[f_name][0]) // Réussite à un challenge
	{
		texte.innerHTML = 'Bravo ! Flag validé !';
        texte.style.color = 'black';
        if(flags[f_name][2]===0) // Réussite du dernier challenge
		{
			document.getElementById('Final').hidden = false;
		}
		else
		{
			const val_part = flags[f_name][1]+flags[f_name][2];
			document.getElementById('Partie'+val_part.toString()).hidden = false;
		}
	}
	else 
	{
		texte.innerHTML = 'Flag incorrect ! Réessayez !';
        texte.style.color = 'red';
	}
}

// formulaires commencent par form_. formulaire de la forme : form_ + clé du dictionnaire flags
form_art_expo1.addEventListener( "submit", check_and_update);
form_art_expo2.addEventListener( "submit", check_and_update);
form_art_expo3.addEventListener( "submit", check_and_update);
form_art_expo4.addEventListener( "submit", check_and_update);
form_art_expo5.addEventListener( "submit", check_and_update);
form_art_expo6.addEventListener( "submit", check_and_update);
form_art_expo7.addEventListener( "submit", check_and_update);
form_art_expo8.addEventListener( "submit", check_and_update);
form_art_expo9.addEventListener( "submit", check_and_update);
form_art_expo10.addEventListener( "submit", check_and_update);
form_art_expo11.addEventListener( "submit", check_and_update);
form_art_expo12.addEventListener( "submit", check_and_update);