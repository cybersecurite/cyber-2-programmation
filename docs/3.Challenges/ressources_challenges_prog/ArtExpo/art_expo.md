---
hide:
  - toc
author: à compléter
title: Art Expo
---

# Challenge : art expo

![](../art.png){: .center }

L'exposition d'art la plus prestigieuse est sur le point de débuter à la Tate Modern de Londres, au Royaume-Uni. 

Pour les besoins de cette exposition, des peintures du monde entier ont été transportées en toute sécurité jusqu'à Londres à l'aide d'un système de protection de pointe appelé multi-safe.

Le multi-coffre contient douze compartiments individuels verrouillés, suffisamment grands pour stocker chacun un tableau. Chaque compartiment est verrouillé à l'aide d'un système de verrouillage à combinaison rotatif. Chaque combinaison est composée de trois nombres.

Pour des raisons de sécurité, les combinaisons réelles sont gardées secrètes et le responsable de la galerie n'a eu accès qu'à douze puzzles de codage qui seront nécessaires pour récupérer les combinaisons et déverrouiller les douze compartiments multi-coffres.

!!! note "Votre objectif"
    Le galeriste a besoin de vous pour l’aider à accéder aux douze tableaux les plus célèbres du monde avant l’inauguration de l’exposition Expo Art.

    Chaque puzzle de codage fournit une liste de trois valeurs. Le flag est la concaténation de ces trois valeurs.

    Par exemple, si un puzzle de codage fournit la liste `[15, 5, 29]`, alors le flag est `15529`.

!!! warning "Attention"
    Pour que ce challenge ait tout son intérêt, il est **recommandé** de ne copier-coller et d'exécuter sur machine les codes Python qui sont proposés. Ceux-ci doivent être exécutés **à la main**.

<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Puzzle 1</h2>

    ```python
    numbers = [7, 14, 16, 9, 24, 30]
    value1 = numbers[0]
    value2 = numbers[2]
    value3 = numbers[4]
    code1 = [value1, value2, value3]
    ```

<p>
    <form id="form_art_expo1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>

<div id='Partie2' hidden>
<br>
<h2>Puzzle 2</h2>

    ```python
    numbers = [18, 15, 6, 21, 32]
    length = len(numbers)
    value1 = numbers[0]
    value2 = numbers[length-1]
    code2 = [value1, value2, length]
    ```

<p>
    <form id="form_art_expo2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>

<div id='Partie3' hidden>
<br>
<h2>Puzzle 3</h2>

    ```python
    list = [22, 10, 31, 19, 8, 17]
    if list[0]>list[1]:
        tmp = list[0]
        list[0] = list[1]
        list[1] = tmp       
    code3 = [list[0], list[1], list[2]]
    ```

<p>
    <form id="form_art_expo3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>

<div id='Partie4' hidden>
<br>
<h2>Puzzle 4</h2>
    ```python
    set1 = [5, 10, 15, 20, 25, 30]
    set2 = [3, 6, 9, 12, 15, 18]
    code4 = []
    for i in range(6):
        if i<3 :
            value = set1[i] + set2[i]
            code4.append(value)
    ```

<p>
    <form id="form_art_expo4">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte4'></p>
</div>

<div id='Partie5' hidden>
<br>
<h2>Puzzle 5</h2>
    ```python
    numbers = [17, 28, 6, 9, 32, 8, 35]
    code5 = []
    for i in range(len(numbers)):
        value = numbers[i]
        if value%2 == 1:
            code5.append(value)
    ```

<p>
    <form id="form_art_expo5">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte5'></p>
</div>

<div id='Partie6' hidden>
<br>
<h2>Puzzle 6</h2>
    ```python
    numbers = [3, 12, 7, 23, 14, 11, 12]
    code6 = []
    for i in range(len(numbers)-1):
        if numbers[i+1] > numbers[i] :
            code6.append(numbers[i+1])
            
    ```

<p>
    <form id="form_art_expo6">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte6'></p>
</div>

<div id='Partie7' hidden>
<br>
<h2>Puzzle 7</h2>
    ```python
    array = [18, 12, 6, 9, 5, 34, 17]
    code7 = []
    for i in range(2, 5) :
        value = array[i] * i
        code7.append(value)
    ```

<p>
    <form id="form_art_expo7">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte7'></p>
</div>

<div id='Partie8' hidden>
<br>
<h2>Puzzle 8</h2>
    ```python
    myList = [2, 5, 7, 8, 1, 3, 6, 9, 8]
    code8 = []
    for i in range(0, 9, 4):
        code8.append(myList[i])
    ```

<p>
    <form id="form_art_expo8">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte8'></p>
</div>

<div id='Partie9' hidden>
<br>
<h2>Puzzle 9</h2>
    ```python
    numbers = [7, 14, 6, 9, 24, 12]
    code9 = []
    for i in range(5, 0, -1):
        if i>2: 
            value = numbers[i] 
            code9.append(value)
    ```

<p>
    <form id="form_art_expo9">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte9'></p>
</div>

<div id='Partie10' hidden>
<br>
<h2>Puzzle 10</h2>
    ```python
    digits = [[7, 8, 9], [4, 5, 6], [1, 2, 3]]
    code10 = []
    for i in range(0, 3):
        code10.append(digits[i][0])
    ```

<p>
    <form id="form_art_expo10">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte10'></p>
</div>

<div id='Partie11' hidden>
<br>
<h2>Puzzle 11</h2>
    ```python
    numbers = [[7, 8, 9], [4, 5, 6], [1, 2, 3]]
    code11 = []
    for i in range(0, 3):
        code11.append(numbers[i][i])
    ```

<p>
    <form id="form_art_expo11">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte11'></p>
</div>

<div id='Partie12' hidden>
<br>
<h2>Puzzle 12</h2>
    ```python
    numbers = [[7, 8, 9], [4, 5, 6], [1, 2, 3]]
    code12 = []
    for i in range(0, 3):
        value = 0
        for j in range(0, 3):
            value = value + numbers[i][j]
        code12.append(value)
    ```

<p>
    <form id="form_art_expo12">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte12'></p>
</div>

<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez réussi à terminer les douze puzzles de codage  !
<br><br>  
Grâce à vous, l'exposition va pouvoir avoir lieu !</p>

!!! danger "A retenir"

    Lire, comprendre et exécuter un programme informatique à la main est une composante essentielle de l'apprentissage à la programmation et, plus largement, à l'informatique.

    Dans la cybersécurité, cette composante se retrouve dans le _reverse engineering_, ou _rétro-ingénierie_ en français, dont l'objectif est l'étude et l'analyse d'un système ou d'un programme informatique pour en déduire son fonctionnement.

</div>

<script src='../script_chall_art_expo.js'></script>