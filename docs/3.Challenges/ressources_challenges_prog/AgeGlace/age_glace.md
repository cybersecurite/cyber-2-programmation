---
hide:
  - toc
author: à compléter
title: Age glaciaire
---

# Challenge : âge glaciaire

![](../age_glace.png){: .center }

Sur la banquise, vivait un pingouin curieux nommé Pipou. Un jour, il découvrit un mystérieux parchemin indiquant &laquo; _l'Île du Trésor_ &raquo;. Déterminé, Pipou s'embarqua vers l'inconnu. Après sept jours de voyage, il atteignit une île recouverte de neige où il découvrit un coffre caché et fermé par un cadenas à trois molettes. Sur chacune de ces molettes était gravé un puzzle de codage.

!!! note "Votre objectif"
    Résoudre les trois puzzles afin d'aider Pipou à ouvrir le coffre.
     

!!! warning "Attention"
    Pour que ce challenge ait tout son intérêt, il est **recommandé** de ne pas exécuter sur machine les codes Scratch qui sont proposés. Ceux-ci doivent être exécutés **à la main**.

??? abstract "Puzzle 1"
    <center><img src='../1. capture.png' style='width:30%;height:auto;'></center>

??? abstract "Puzzle 2"
    <center><img src='../2. capture.png' style='width:30%;height:auto;'></center>

??? abstract "Puzzle 3"
    <center><img src='../3. capture.png' style='width:30%;height:auto;'></center>


<hr style="height:5px;color:red;background-color:red;">
<center>A vous de jouer !</center>
<hr style="height:5px;color:red;background-color:red;">

<div id='Partie1'>
<h2>Puzzle 1</h2>

Quel chiffre obtient-on après l'exécution du puzzle 1 ?

<p>
    <form id="form_age_glace1">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte1'></p>
</div>

<div id='Partie2' hidden>
<br>
<h2>Puzzle 2</h2>

Quelle figure obtient-on après l'exécution du puzzle 2 ? (on attend ici le nom de la figure en toute lettre)


<p>
    <form id="form_age_glace2">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte2'></p>
</div>

<div id='Partie3' hidden>
<br>
<h2>Puzzle 3</h2>

Quelle est la valeur de la variable <code>pas</code> dans le puzzle 3 ?


<p>
    <form id="form_age_glace3">
        <label for="flag">Flag : </label>
        <input type="text" name="flag" style='background-color:black;color:white;' size=50>
        <input type="submit" value="Vérifier">
    </form>
</p>
    <p id='Texte3'></p>
</div>

<div id='Final' markdown="1" hidden>
<br>
<h2>Félicitations !</h2>
<p>Vous avez réussi à terminer les trois puzzles de codage  !
<br><br>  
Avec un battement de coeur, Pipou ouvrit le coffre pour y trouver un cristal étincelant et deux mots gravés : <span style='font-style:italic;'>Amour</span> et <span style='font-style:italic;'>Aventure</span>. Il comprit alors que le plus grand trésor était l'aventure et l'amour qu'il découvrait chaque jour.</p>

!!! danger "A retenir"

    Lire, comprendre et exécuter un programme informatique à la main est une composante essentielle de l'apprentissage à la programmation et, plus largement, à l'informatique.

    Dans la cybersécurité, cette composante se retrouve dans le _reverse engineering_, ou _rétro-ingénierie_ en français, dont l'objectif est l'étude et l'analyse d'un système ou d'un programme informatique pour en déduire son fonctionnement.

</div>

<script src='../script_chall_age_glace.js'></script>